import path from 'path';
import React from 'react';
import express from 'express';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';

import App from '../../src/common/components/App.jsx';
import configureStore from '../../src/common/store/configureStore';
import donations from '../common/modules/donations';
import charity from '../common/modules/charity';
import { ports } from '../../config';
import markup from './markup';

const app = express();
const port = process.env.NODE_ENV === 'test' ? ports.test : ports.app;

const handleRender = (req, res) => {
  const store = configureStore();
  const populateStore = [
    store.dispatch(donations.loadDonations()),
    store.dispatch(charity.loadCharity()),
  ];

  Promise.all(populateStore)
    .then(() => {
      const html = renderToString(
        <Provider store={store}>
          <App />
        </Provider>
      );
      const preloadedState = store.getState();
      res.send(markup(html, preloadedState));
    });
};

app.get('/', handleRender);
app.use('/static', express.static(path.join(__dirname, '../../dist')));

app.listen(
  port,
  () => console.log(`Just Giving App running at 🌍 http://localhost:${port}`) // eslint-disable-line no-console
);
export default app;
