const markup = (html, preloadedState) =>
  `<!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        <link href="/static/css/main.css" rel="stylesheet">
        <title>Just Giving Donations</title>
    </head>
    <body>
      <div id="app">${html}</div>
      <script>window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\x3c')}</script>
      <script src="/static/js/bundle.js"></script>
    </body>
  </html>
  `;

export default markup;
