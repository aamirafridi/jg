/* eslint-disable import/imports-first */
require('es6-promise').polyfill();
require('isomorphic-fetch');

import { CHARITY_LOADED, CHARITY_ERROR } from './constants';
import { charityApi } from '../../../../config';

const headers = { headers: { 'Content-Type': 'application/json' } };

const charityLoaded = (payload) => ({
  type: CHARITY_LOADED,
  payload,
});

const charityError = (payload) => ({
  type: CHARITY_ERROR,
  payload,
});

const loadCharity = () => (dispatch) =>
 fetch(charityApi, headers)
  .then((charity) => charity.json())
  .then((charity) => dispatch(charityLoaded(charity)))
  .catch((err) => dispatch(charityError({ err })));

module.exports = {
  loadCharity,
  charityLoaded,
  charityError,
};
