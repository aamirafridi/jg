import { combineReducers } from 'redux';
import { CHARITY_LOADED } from './constants';

function charity(state = {}, action) {
  switch (action.type) {
    case CHARITY_LOADED: {
      const { name, description, logoAbsoluteUrl, profilePageUrl, websiteUrl } = action.payload;
      const charityDetails = { name, description, logoAbsoluteUrl, profilePageUrl, websiteUrl };
      return {
        ...state,
        ...charityDetails,
      };
    }
    default: return state;
  }
}

module.exports = combineReducers({
  charity,
});
