import { createSelector } from 'reselect';

const getRootSelector = createSelector(
  (state) => state,
  ({ charity }) => charity
);

const getCharity = createSelector(
  getRootSelector,
  ({ charity }) => charity
);

module.exports = {
  getCharity,
};
