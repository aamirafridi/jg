import keyMirror from 'keymirror';

module.exports = keyMirror({
  CHARITY_LOADED: null,
  CHARITY_ERROR: null,
});
