import forEach from 'lodash/forEach';

import rootReducer from './reducers';
import constants from './constants';
import actions from './actions';
import selectors from './selectors';

const moduleProps = Object.assign({}, actions, selectors, constants);

forEach(moduleProps, (prop, propName) => { rootReducer[propName] = prop; });

module.exports = rootReducer;
