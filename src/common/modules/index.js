import { combineReducers } from 'redux';
import donations from './donations';
import charity from './charity';

module.exports = combineReducers({ donations, charity });
