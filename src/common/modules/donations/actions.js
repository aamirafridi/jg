/* eslint-disable import/imports-first */
require('es6-promise').polyfill();
require('isomorphic-fetch');

import { DONATIONS_LOADED, DONATIONS_ERROR } from './constants';
import { charityDonationsApi } from '../../../../config';

const headers = { headers: { 'Content-Type': 'application/json' } };

const donationsLoaded = (payload) => ({
  type: DONATIONS_LOADED,
  payload,
});

const donationsError = (payload) => ({
  type: DONATIONS_ERROR,
  payload,
});

const loadDonations = () => (dispatch) =>
 fetch(charityDonationsApi, headers)
  .then((donations) => donations.json())
  .then((donations) => dispatch(donationsLoaded(donations)))
  .catch((err) => dispatch(donationsError({ err })));

module.exports = {
  loadDonations,
  donationsLoaded,
  donationsError,
};
