import keyMirror from 'keymirror';

module.exports = keyMirror({
  DONATIONS_LOADED: null,
  DONATIONS_ERROR: null,
});
