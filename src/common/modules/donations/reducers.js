import moment from 'moment'; // bad idea to add this lib just for simple thing but just to save time

import { combineReducers } from 'redux';
import { DONATIONS_LOADED } from './constants';

const getDisplayDate = (date) =>
  // cannot understand the date format so using split for now
  moment(Number(date.split('(')[1].split('+')[0]))
    .format('MMMM Do YYYY, h:mm:ss a');

function donations(state = [], action) {
  switch (action.type) {
    case DONATIONS_LOADED: {
      return [
        ...state,
        ...action.payload.donations.map(({
          currencyCode,
          amount,
          donorDisplayName,
          donationDate,
          message,
          imageUrl,
        }) => ({
          donorDisplayName,
          amount: `${currencyCode} ${amount}`,
          donationDate: getDisplayDate(donationDate),
          message,
          imageUrl,
        })),
      ];
    }
    default: return state;
  }
}

module.exports = combineReducers({
  donations,
});
