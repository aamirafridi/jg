import { createSelector } from 'reselect';

const getRootSelector = createSelector(
  (state) => state,
  ({ donations }) => donations
);

const getDonations = createSelector(
  getRootSelector,
  ({ donations }) => donations
);

module.exports = {
  getDonations,
};
