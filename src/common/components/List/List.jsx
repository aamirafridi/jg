import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import donations from '../../modules/donations';

class List extends Component {
  static propTypes = {
    /* state */
    donations: PropTypes.arrayOf(
      PropTypes.shape({
        donorDisplayName: PropTypes.string.isRequired,
        amount: PropTypes.string.isRequired,
        donationDate: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired,
        imageUrl: PropTypes.string.isRequired,
      })
    ).isRequired,
  }

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const donationsList = this.props.donations.map(({
      donorDisplayName,
      amount,
      donationDate,
      message,
      imageUrl
    }, i) => {
      return (
        <li key={i} className="donations-list__item t-donations-list__item">
          <div className="donations-list__avatar pull-left">
            <img src={imageUrl} />
          </div>
          <b className="donations-list__name">{ donorDisplayName }</b>
          <div className="donations-list__details clearfix">
            Donated <b className="donations-list__amount">{amount}</b> on {donationDate}
            <p className="no-margin">&quot;<i>{message}</i>&quot;</p>
          </div>
        </li>
      );
    })
    return (
      <div className='donations-list t-donations-list'>
        <ul className="list-unstyled">{ donationsList }</ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  donations: donations.getDonations(state),
});

export default connect(mapStateToProps)(List);
