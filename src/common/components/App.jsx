import React, { PropTypes } from 'react';
import Charity from './Charity/Charity.jsx';
import List from './List/List.jsx';

export default function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <Charity/>
        </div>
        <div className="col-md-12">
          <List/>
        </div>
      </div>
    </div>
  );
};
