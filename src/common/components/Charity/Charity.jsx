import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import charity from '../../modules/charity';

class Charity extends Component {
  static propTypes = {
    /* state */
    charity: PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      logoAbsoluteUrl: PropTypes.string.isRequired,
      profilePageUrl: PropTypes.string.isRequired,
      websiteUrl: PropTypes.string.isRequiredl
    }).isRequired,
  }

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {
      name,
      description,
      logoAbsoluteUrl,
      profilePageUrl,
      websiteUrl,
    } = this.props.charity;
    return (
      <div className='charity-details t-charity-details'>
        <div className="row charity-details__wrapper">
          <div className="col-xs-12">
            <img className="charity-details__img pull-left" src={logoAbsoluteUrl}/>
            <h1><a href={profilePageUrl}>{name}</a></h1>
            <p className="charity-details__desc">{description}</p>
            <i className="glyphicon glyphicon-link" />
            <a href={websiteUrl} target="_blank">{websiteUrl}</a>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  charity: charity.getCharity(state),
});

export default connect(mapStateToProps)(Charity);
