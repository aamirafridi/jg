/* eslint-disable no-unused-vars, no-undef, import/no-extraneous-dependencies*/
import chai from 'chai';
import httpStatusCode from 'http-status-codes';
import supertest from 'supertest-as-promised';
import cheerio from 'cheerio';
import app from '../../src/server/server';

const { expect } = chai;

describe('Donations', () => {
  let request;

  before((done) => {
    request = supertest(app);
    done();
  });

  it('Should render charity title and list of donations', () =>
    request
      .get('/')
      .expect(httpStatusCode.OK)
      .then((res) => {
        const $ = cheerio.load(res.text);
        expect($('.t-charity-details').length).to.equal(1);
        expect($('.t-donations-list__item')).to.have.length.of.at.least(4);
      })
  );
});
