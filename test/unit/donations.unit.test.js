import { default as fakeConfigureStore } from 'redux-mock-store';
import chai from 'chai';
import isEqual from 'lodash/isEqual';
import fetchMock from 'fetch-mock';

import configureStore from '../../src/common/store/configureStore';
import donations from '../../src/common/modules/donations';
import { donationsData, reducedDonationsData } from '../data/donations';
import { charityDonationsApi } from '../../config';

const { expect } = chai;

describe('Donations', () => {
  it('should load', () => {
    fetchMock.get(charityDonationsApi, donationsData);
    const store = configureStore();
    return store.dispatch(donations.loadDonations())
      .then(() =>
        expect(isEqual(store.getState().donations, reducedDonationsData)).to.be.true);
  });

  it('should be loaded', () => {
    const mockStore = fakeConfigureStore([]);
    const initialState = {};
    const store = mockStore(initialState);
    const payload = donationsData;

    store.dispatch(donations.donationsLoaded(payload));
    const actions = store.getActions();
    expect(actions).to.have.length(1);
    expect(actions[0].type).to.equals(donations.DONATIONS_LOADED);
    expect(actions[0].payload.donations).to.have.length(1);
    expect(actions[0].payload.donations[0].amount).to.equals(20);
  });
});
