import { default as fakeConfigureStore } from 'redux-mock-store';
import chai from 'chai';
import isEqual from 'lodash/isEqual';
import fetchMock from 'fetch-mock';

import configureStore from '../../src/common/store/configureStore';
import charity from '../../src/common/modules/charity';
import { charityData, reducedCharityData } from '../data/charity';
import { charityApi } from '../../config';

const { expect } = chai;

describe('Charity details', () => {
  it('should load and reduced', () => {
    fetchMock.get(charityApi, charityData);
    const store = configureStore();
    return store.dispatch(charity.loadCharity())
      .then(() =>
        expect(isEqual(store.getState().charity, reducedCharityData)).to.be.true);
  });

  it('should be loaded', () => {
    const mockStore = fakeConfigureStore([]);
    const initialState = {};
    const store = mockStore(initialState);
    const payload = charityData;

    store.dispatch(charity.charityLoaded(payload));
    const actions = store.getActions();
    expect(actions).to.have.length(1);
    expect(actions[0].type).to.equals(charity.CHARITY_LOADED);
    expect(actions[0].payload.name).to.equals(reducedCharityData.charity.name);
  });
});
