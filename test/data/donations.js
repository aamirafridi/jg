const donationsData = {
  donations: [
    {
      amount: 20,
      currencyCode: 'GBP',
      donationDate: '/Date(1477219802000+0000)/',
      donorDisplayName: 'vJupp',
      donorLocalAmount: 20,
      donorLocalCurrencyCode: 'GBP',
      estimatedTaxReclaim: 5,
      imageUrl: 'https://www.justgiving.com/content/images/graphics/icons/avatars/facebook-avatar.gif',
      message: 'In memory of a lovely man,Paul and I will miss you',
    },
  ],
};

const reducedDonationsData = {
  donations: [
    {
      donorDisplayName: 'vJupp',
      amount: 'GBP 20',
      donationDate: 'October 23rd 2016, 11:50:02 am',
      message: 'In memory of a lovely man,Paul and I will miss you',
      imageUrl: 'https://www.justgiving.com/content/images/graphics/icons/avatars/facebook-avatar.gif',
    },
  ],
};

module.exports = {
  donationsData,
  reducedDonationsData,
};
