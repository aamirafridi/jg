const charityData = {
  name: 'British Heart Foundation',
  description: 'Coronary heart disease is the UK’s single biggest killer but we are leading the fight against it. Your donations power our life-saving research. Help us keep more families together. Join our fight for every heartbeat.',
  logoAbsoluteUrl: 'http://images.justgiving.com/image/8d026099-7e28-4b39-ae86-330d7e471916.gif',
  profilePageUrl: 'https://www.justgiving.com/britishheartfoundation',
  registrationNumber: '225971',
  websiteUrl: 'http://www.bhf.org.uk',
};

const reducedCharityData = {
  charity: {
    name: 'British Heart Foundation',
    description: 'Coronary heart disease is the UK’s single biggest killer but we are leading the fight against it. Your donations power our life-saving research. Help us keep more families together. Join our fight for every heartbeat.',
    logoAbsoluteUrl: 'http://images.justgiving.com/image/8d026099-7e28-4b39-ae86-330d7e471916.gif',
    profilePageUrl: 'https://www.justgiving.com/britishheartfoundation',
    websiteUrl: 'http://www.bhf.org.uk',
  },
};

module.exports = {
  charityData,
  reducedCharityData,
};
