# Just Giving Donations App

* **Time spent:** about 4-5 hours.
* **Description:** Simple universal/isomorphic react/redux (although not needed but just to demonstrate) donations app.
* **Progressive enhancement:** build for server first then client. Works without javascript in browser.
* **Tools & technologies:** React, redux, webpack, express, scss, gulp, es6

## Start
```bash
$ mkdir jg
$ cd jg
$ git clone git@bitbucket.org:aamirafridi/jg.git .
$ nvm use
$ npm install
```

## Run app
Ignore warnings
```bash
$ npm run build-assets && npm run start
```

## Scripts
```bash
$ npm run
    lint
    start
    watch
    build-css
    watch-css
    build-assets
    watch-assets
    precommit
    prepush
    test:server
    test:unit
    test
```

## Hooks
```bash
"precommit": "npm run lint",
"prepush": "npm run test"
```

## URL
`http://localhost:3000`