/* eslint-disable import/no-extraneous-dependencies*/
const path = require('path');
const webpack = require('webpack'); // eslint-disable-line no-unused-vars

module.exports = {
  devtool: 'inline-source-map',
  entry: [
    './src/client/index.jsx',
  ],
  output: {
    path: path.join(__dirname, 'dist', 'js'),
    filename: 'bundle.js',
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: ['babel'],
        include: path.join(__dirname, 'src'),
      },
    ],
  },
};
