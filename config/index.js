module.exports = {
  charityApi: 'https://api.justgiving.com/601866a2/v1/charity/183092',
  charityDonationsApi: 'https://api.justgiving.com/601866a2/v1/charity/183092/donations',
  ports: {
    app: 3000,
    test: 3001,
  },
};
