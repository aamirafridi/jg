/* eslint-disable import/no-extraneous-dependencies*/
import gulp from 'gulp';
import sass from 'gulp-sass';
import rename from 'gulp-rename';

const paths = {
  scssEntry: './src/common/components/App.scss',
  scssWatch: './src/common/**/*.scss',
  dist: 'dist',
};

gulp.task('default', ['build', 'watch']);
gulp.task('build', ['build:sass']);

gulp.task('build:sass', () =>
  gulp.src(paths.scssEntry)
    .pipe(sass({ errLogToConsole: true }))
    .pipe(rename('main.css'))
    .pipe(gulp.dest(`${paths.dist}/css/`))
);

gulp.task('watch', () => {
  gulp.watch([paths.scssWatch], ['build:sass']);
});
